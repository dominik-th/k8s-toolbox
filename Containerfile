FROM registry.fedoraproject.org/fedora-toolbox:37

ENV NAME=k8s-toolbox VERSION=37
LABEL com.github.containers.toolbox="true" \
      com.redhat.component="$NAME" \
      name="$NAME" \
      version="$VERSION" \
      usage="This image is meant to be used with the toolbox command" \
      summary="Base image for creating Fedora toolbox containers" \
      maintainer="Alexander Weidemann <walnuss081@gmail.com>"


RUN sed -i '/tsflags=nodocs/d' /etc/dnf/dnf.conf
RUN dnf -y swap coreutils-single coreutils-full

RUN dnf -y install ca-certificates git curl bash bash-completion vim nano zsh

RUN curl -s -L -o /usr/bin/k3sup https://github.com/alexellis/k3sup/releases/download/0.12.3/k3sup \
 && chmod +x /usr/bin/k3sup

RUN curl -s -L -o /usr/bin/kubectl https://dl.k8s.io/release/v1.25.0/bin/linux/amd64/kubectl \
 && chmod +x /usr/bin/kubectl

RUN curl -s -L https://github.com/derailed/k9s/releases/download/v0.27.4/k9s_Linux_amd64.tar.gz | tar xz -C /usr/bin/ \
 && chmod +x /usr/bin/k9s

RUN curl -s -L https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2Fv4.5.7/kustomize_v4.5.7_linux_amd64.tar.gz | tar xz -C /usr/bin/ \
 && chmod +x /usr/bin/kustomize

RUN curl -s -L https://get.helm.sh/helm-v3.10.0-linux-amd64.tar.gz | tar xz --strip-components=1 -C /usr/bin/ linux-amd64/helm \
 && chmod +x /usr/bin/helm

RUN curl -s -L -o /usr/bin/kubectl https://dl.k8s.io/release/v1.25.0/bin/linux/amd64/kubectl \
 && chmod +x /usr/bin/kubectl

RUN curl -s -L -o /usr/bin/kubectx https://github.com/ahmetb/kubectx/releases/download/v0.9.4/kubectx \
 && chmod +x /usr/bin/kubectx

RUN curl -s -L -o /usr/bin/kubens https://github.com/ahmetb/kubectx/releases/download/v0.9.4/kubens \
 && chmod +x /usr/bin/kubens

RUN curl -s -L https://github.com/bitnami-labs/sealed-secrets/releases/download/v0.18.5/kubeseal-0.18.5-linux-amd64.tar.gz | tar xz -C /usr/bin/  \
 && chmod +x /usr/bin/kubeseal

RUN curl -s -L -o /usr/bin/yq https://github.com/mikefarah/yq/releases/download/v4.34.1/yq_linux_amd64 \
 && chmod +x /usr/bin/yq

RUN curl -s -L https://github.com/google/go-jsonnet/releases/download/v0.20.0/go-jsonnet_0.20.0_Linux_x86_64.tar.gz | tar xz -C /usr/bin/ \
 && chmod +x /usr/bin/jsonnet /usr/bin/jsonnetfmt /usr/bin/jsonnet-lint

RUN curl -s -L -o /usr/bin/jb https://github.com/jsonnet-bundler/jsonnet-bundler/releases/download/v0.5.1/jb-linux-amd64 \
 && chmod +x /usr/bin/jb

RUN curl -s -L -o /usr/bin/talosctl  https://github.com/siderolabs/talos/releases/download/v1.4.6/talosctl-linux-amd64 \
 && chmod +x /usr/bin/talosctl

RUN dnf clean all
