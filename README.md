# k8s Toolbox

## Build
```bash
podman build -f Containerfile
```

## Run
```bash
toolbox create --image registry.gitlab.com/walnuss0815/k8s-toolbox:latest k8s-toolbox
```
